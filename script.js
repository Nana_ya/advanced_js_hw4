let request = fetch("https://ajax.test-danit.com/api/swapi/films");

request.then((result) => result.json())
.then((data) => {
    let ul = document.createElement("ul");
    data.forEach(el => {
        let li = document.createElement("li");
        let liID = document.createElement("p");
        liID.textContent = "Id of the film: " + el.episodeId;
        li.append(liID);
    
        let liName = document.createElement("p");
        liName.setAttribute('id', 'name');
        liName.textContent = "Name of the film: " + el.name;
        li.append(liName);
        ul.append(li);
        let charactersList = el.characters;
        let ulCharacters = document.createElement("ul");
        let liCharacters = document.createElement("p");
        liCharacters.textContent = "Characters of the film: ";
        ul.append(liCharacters);
        charactersList.forEach(el => {
            fetch(el).then((result) => result.json())
            .then((data) => {
                let liCharacterName = document.createElement("li");
                liCharacterName.textContent = data.name;
                ulCharacters.append(liCharacterName);
            })
        })
        ul.append(ulCharacters);

        let liOpeningCrawl = document.createElement("p");    
        liOpeningCrawl.textContent = "Opening crawl of the film: " + el.openingCrawl;
        ul.append(liOpeningCrawl);
    });
    document.body.appendChild(ul);
})

  